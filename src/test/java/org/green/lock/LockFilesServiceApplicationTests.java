package org.green.lock;

import org.green.lock.service.FileLockingService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class LockFilesServiceApplicationTests {

    @Autowired
    private FileLockingService fileLockingService;

    @Test
    void contextLoads() {
        this.fileLockingService.removeFile(
                "D:\\Users\\sm13\\Documents\\development\\ipf\\be\\branch_develop\\area-search\\ipf-area-search\\itests\\target\\exam\\451c0dc5-b433-40e9-adb6-c8ccdc51539b\\log\\karaf.log" +
                "");
    }

    @Test
    public void testInsert() {
        String[] columnNames = {"column1", "column2"};
        Object[] valueObjs = {"value1", 123};
        Class[] dataTypes = {String.class, Integer.class};
        insert("test1", columnNames, valueObjs, dataTypes);
    }

    public void insert(String tableName, String[] columnNames, Object[] valueObjs, Class[] dataTypes) {
        if (valueObjs.length != dataTypes.length) {
            throw new IllegalArgumentException("Data type count and value count does not match!");
        }
        String columns = "";
        for (String column : columnNames) {
            columns = columns + column + ", ";
        }
        columns = columns.substring(0, columns.length() - 2);


        String values = formatValue(valueObjs, dataTypes);


        String statement = "INSERT INTO \"" + tableName + "\" (" + columns + ") VALUES (" + values + ")";
        System.err.println(statement);
    }

    private String formatValue(Object[] valueObjs, Class[] dataTypes) {
        String values = "";
        for (int i = 0; i < valueObjs.length; i++) {
            Class dataType = dataTypes[i];
            Object value = valueObjs[i];

            if (dataType == String.class) {
                values = values + "'" + value + "', ";
                System.err.println("1. dataType:");
            }
            else if (dataType == Integer.class) {
                values = values + value + ", ";
                System.err.println("2. dataType:");
            }
        }
        values = values.substring(0, values.length() - 2);
        return values;
    }

//	@Test
//	public void testRemoveDir() throws IOException {
//		String fileLocation = "D:\\Users\\user\\Documents\\development";
//		File fileDirectories = new File(fileLocation);
//
//		try (Stream<Path> stream = Files.find(Paths.get(fileDirectories.getAbsolutePath()),
//											  20,
//											  (path, attr) -> (path.getFileName().toString().contains(".iml") ||
//															   path.getFileName().toString().contains(".idea")))) {
//			stream.forEach(path -> {
//				try {
//					System.err.println(path);
//					Files.delete(path);
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			});
//		}
//	}

}
