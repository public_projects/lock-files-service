package org.green.lock.controller;

import lombok.extern.slf4j.Slf4j;
import org.green.lock.service.FileLockingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.Set;


@Slf4j
@RestController
public class LockFilesController {

    public static final String LOCK_FILE = "/lock/file";
    public static final String UNLOCK_FILE = "/unlock/file";
    public static final String ALL_LOCK_FILE = "/lock/files";
    public static final String UNLOCK_ALL = "/unlock/all";
    public static final String IS_FILE_LOCK = "/is/lock/";

    @Autowired
    private FileLockingService fileLockingService;

    @CrossOrigin
    @GetMapping(LOCK_FILE)
    public ResponseEntity<String> getLock(@RequestParam(value = "file", required = true) String filePath) {
        StopWatch watch = new StopWatch();
        watch.start();

        try {
            fileLockingService.lockFile(filePath);
        }
        catch (Exception ex) {
            watch.stop();
            log.info("Failed {} within {} ms", LOCK_FILE, watch.getTotalTimeMillis());
            ex.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(ex.getMessage());
        }
        watch.stop();
        log.info("Completed {} within {} ms", LOCK_FILE, watch.getTotalTimeMillis());
        return ResponseEntity.accepted().body("File " + filePath + " locked!");
    }

    @CrossOrigin
    @GetMapping(UNLOCK_ALL)
    public ResponseEntity<String> getUnlockAll() {
        StopWatch watch = new StopWatch();
        watch.start();

        Set<String> files = new HashSet<>();
        try {
            fileLockingService.unlockAll();
        }
        catch (Exception ex) {
            watch.stop();
            log.info("Failed {} within {} ms", UNLOCK_ALL, watch.getTotalTimeMillis());
            ex.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(ex.getMessage());
        }
        watch.stop();
        log.info("Completed {} within {} ms", UNLOCK_ALL, watch.getTotalTimeMillis());
        return ResponseEntity.accepted().body(files.toString());
    }

    @CrossOrigin
    @GetMapping(UNLOCK_FILE)
    public ResponseEntity<String> getUnlockLock(@RequestParam(value = "file", required = true) String filePath) {
        StopWatch watch = new StopWatch();
        watch.start();

        try {
            fileLockingService.releaseLock(filePath);
        }
        catch (Exception ex) {
            watch.stop();
            log.info("Failed {} within {} ms", LOCK_FILE, watch.getTotalTimeMillis());
            ex.printStackTrace();
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(ex.getMessage());
        }
        watch.stop();
        log.info("Completed {} within {} ms", UNLOCK_FILE, watch.getTotalTimeMillis());
        return ResponseEntity.accepted().body("File " + filePath + " unlocked!");
    }

    @CrossOrigin
    @GetMapping(ALL_LOCK_FILE)
    public ResponseEntity<Set<String>> getLockFiles() {
        StopWatch watch = new StopWatch();
        watch.start();

        Set<String> files = new HashSet<>();
        files = fileLockingService.getLockFiles();

        watch.stop();
        log.info("Completed {} within {} ms", ALL_LOCK_FILE, watch.getTotalTimeMillis());
        return ResponseEntity.accepted().body(files);
    }

    @CrossOrigin
    @GetMapping(IS_FILE_LOCK)
    public ResponseEntity<Set<String>> isFileLocked(@RequestParam(value = "file", required = true) String filePath) {
        StopWatch watch = new StopWatch();
        watch.start();

        Set<String> files = new HashSet<>();
        files = fileLockingService.getLockFiles();

        watch.stop();
        log.info("Completed {} within {} ms", ALL_LOCK_FILE, watch.getTotalTimeMillis());
        return ResponseEntity.accepted().body(files);
    }
}
