package org.green.lock.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.javatuples.Pair;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.util.*;


@Slf4j
@Service
public class FileLockingService {
    private Map<String, Pair<FileChannel, FileLock>> lockedTriplets = new HashMap<>();

    public void lockFile(String filePath) throws Exception {
        if (lockedTriplets.get(filePath) != null) {
            throw new Exception("This file already been locked before!");
        }
        FileChannel fileChannel = new RandomAccessFile(filePath, "rw").getChannel();
        FileLock fileLock = fileChannel.lock();
        Pair<FileChannel, FileLock> fileLockPair = new Pair<>(fileChannel, fileLock);
        lockedTriplets.put(filePath, fileLockPair);
    }

    public void removeFile(String filePath) {
        File directory = new File(filePath);

        try {
            FileUtils.forceDelete(directory);
            System.err.println("Deleted file: "+ directory);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void releaseLock(String filePath) throws Exception {
        Pair<FileChannel, FileLock> fileLockPair = lockedTriplets.remove(filePath);
        if (fileLockPair != null) {
            fileLockPair.getValue1().release();
            fileLockPair.getValue0().close();
        }
    }

    public Set<String> getLockFiles() {
        return lockedTriplets.keySet();
    }

    public Set<String> unlockAll() {
        Set<String> fileLocations = new HashSet<>();
        lockedTriplets.forEach((s, fileLockPair) -> {
            try {
                fileLocations.add(s);
                fileLockPair.getValue1().release();
                fileLockPair.getValue0().close();
            }
            catch (Exception ex) {
                log.error("Failed to unlock {}", s, ex);
            }
        });
        lockedTriplets = new HashMap<>();
        return fileLocations;
    }

    public boolean isFileLocked(String filePath) {
        if (lockedTriplets.get(filePath) != null) return true;
        return false;
    }


    public void logFile(String filePath) {
        try {

            File file = new File(filePath);

            // Creates a random access file stream to read from, and optionally to write to
            FileChannel channel = new RandomAccessFile(file, "rw").getChannel();

            // Acquire an exclusive lock on this channel's file (blocks until lock can be retrieved)
            FileLock lock = channel.lock();

            // Attempts to acquire an exclusive lock on this channel's file (returns null or throws
            // an exception if the file is already locked.
            try {

                lock = channel.tryLock();
            }
            catch (OverlappingFileLockException e) {
                // thrown when an attempt is made to acquire a lock on a a file that overlaps
                // a region already locked by the same JVM or when another thread is already
                // waiting to lock an overlapping region of the same file
                System.out.println("Overlapping File Lock Error: " + e.getMessage());
            }

            // release the lock
            lock.release();

            // close the channel
            channel.close();

        }
        catch (IOException e) {
            System.out.println("I/O Error: " + e.getMessage());
        }
    }
}
