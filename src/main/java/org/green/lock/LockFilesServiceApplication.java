package org.green.lock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LockFilesServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(LockFilesServiceApplication.class, args);
	}

}
